import test from 'ava';

import merge from '../lib/merge.js';

test(
    'json -> json',
    t => {
        t.deepEqual(
            merge({
                outputType: 'json',
                inputs: [
                    {
                        inputType: 'json',
                        inputValue: {
                            'foo': 'bar'
                        }
                    },
                    {
                        inputType: 'json',
                        inputValue: {
                            'baz': 'fum'
                        }
                    }
                ]
            }),
            {
                'foo': 'bar',
                'baz': 'fum'
            }
        );
    }
);