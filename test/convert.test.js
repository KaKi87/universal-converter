import test from 'ava';

import convert, { types } from '../lib/convert.js';

const
    text = 'Lorem ipsum dolor sit amet',
    samples = {
        'base.base10': 1234567890,
        'base.base2': '1001001100101100000001011010010',
        text,
        'base64': 'TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQ=',
        'date.milliseconds': 1663704542000,
        'date.seconds': 1663704542,
        'date.text': '2022-09-20T22:09:02+02:00Z',
        'jwt': 'eyJhbGciOiJIUzI1NiJ9.eyJ0ZXh0IjoiTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQiLCJpYXQiOjE2NjM3MDQ1NDJ9.IVSKE865T9a6MhX1-pfGUeykdVnWC6P9FR7N3Xemo6k',
        'json': { text }
    },
    inputSamples = {
        ...samples
    },
    outputSamples = {
        ...samples,
        'json': {
            text,
            'iat': samples['date.seconds']
        }
    },
    options = {
        'date.text': ['YYYY-MM-DDTHH:mm:ssZ[Z]'],
        'jwt': [
            ['setProtectedHeader', [{ 'alg': 'HS256' }]],
            ['setIssuedAt', [samples['date.seconds']]],
            ['sign', [new TextEncoder().encode(text)]]
        ]
    },
    inputOptions = {
        ...options,
        'jwt': options['jwt'][2][1]
    },
    outputOptions = {
        ...options
    };

for(const [inputType, inputSubtypes] of Object.entries(types)){
    for(const [inputSubtype, outputTypes] of Object.entries(inputSubtypes)){
        for(const [outputType, outputSubtypes] of Object.entries(outputTypes)){
            for(const outputSubtype of Object.keys(outputSubtypes)){
                const
                    inputKey = `${inputType}${inputSubtype ? `.${inputSubtype}` : ''}`,
                    outputKey = `${outputType}${outputSubtype ? `.${outputSubtype}` : ''}`;
                test(
                    `${inputKey} -> ${outputKey}`,
                    async t => {
                        t.deepEqual(
                            await convert({
                                outputType,
                                outputSubtype,
                                outputOptions: outputOptions[outputKey],
                                inputType,
                                inputSubtype,
                                inputOptions: inputOptions[inputKey],
                                inputValue: inputSamples[inputKey]
                            }),
                            outputSamples[outputKey]
                        );
                    }
                );
            }
        }
    }
}