import test from 'ava';

import generate, { types } from '../lib/generate.js';

for(const [outputType, outputSubtypes] of Object.entries(types)){
    for(const outputSubtype of Object.keys(outputSubtypes)){
        test(
            `${outputType}${outputSubtype ? `.${outputSubtype}` : ''}`,
            t => {
                t.truthy(
                    generate({
                        outputType,
                        outputSubtype
                    })
                )
            }
        );
    }
}