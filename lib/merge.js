export default ({
    outputType,
    outputSubtype,
    outputOptions,
    inputs
}) => {
    let outputValue;
    switch(outputType){
        case 'json': {
            outputValue = {};
            for(const {
                inputType,
                inputSubtype,
                inputOptions,
                inputValue
            } of inputs){
                switch(inputType){
                    case 'json': {
                        Object.assign(outputValue, inputValue);
                        break;
                    }
                }
            }
            break;
        }
    }
    if(typeof outputValue === 'undefined')
        throw new Error('Not implemented');
    else
        return outputValue;
};

export const types = {
    'json': {
        '': {
            'json': {
                '': true
            }
        }
    }
};