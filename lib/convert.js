import base64 from 'base64it';
import dayjs from 'dayjs';
import dayjsCustomParseFormat from 'dayjs/plugin/customParseFormat.js';
import {
    jwtVerify,
    SignJWT
} from 'jose';

dayjs.extend(dayjsCustomParseFormat);

export default async ({
    outputType,
    outputSubtype,
    outputOptions = [],
    inputType,
    inputSubtype,
    inputOptions = [],
    inputValue
}) => {
    let outputValue;
    switch(outputType){
        case 'base': {
            switch(outputSubtype){
                case 'base10': {
                    switch(inputType){
                        case 'base': {
                            switch(inputSubtype){
                                case 'base2': {
                                    outputValue = parseInt(inputValue, 2);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
                case 'base2': {
                    switch(inputType){
                        case 'base': {
                            switch(inputSubtype){
                                case 'base10': {
                                    outputValue = Number(inputValue).toString(2);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }
            break;
        }
        case 'base64': {
            switch(inputType){
                case 'text': {
                    outputValue = base64.encode(inputValue, ...outputOptions);
                    break;
                }
            }
            break;
        }
        case 'date': {
            switch(inputType){
                case 'date': {
                    switch(inputSubtype){
                        case 'milliseconds':
                        case 'text': {
                            inputValue = dayjs(inputValue, ...inputOptions);
                            break;
                        }
                        case 'seconds': {
                            inputValue = dayjs.unix(inputValue);
                            break;
                        }
                    }
                    break;
                }
            }
            if(inputValue){
                switch(outputSubtype){
                    case 'milliseconds': {
                        outputValue = inputValue.valueOf();
                        break;
                    }
                    case 'seconds': {
                        outputValue = inputValue.unix();
                        break;
                    }
                    case 'text': {
                        outputValue = inputValue.format(...outputOptions);
                        break;
                    }
                }
            }
            break;
        }
        case 'json': {
            switch(inputType){
                case 'jwt': {
                    outputValue = (await jwtVerify(inputValue, ...inputOptions)).payload;
                    break;
                }
            }
            break;
        }
        case 'jwt': {
            switch(inputType){
                case 'json': {
                    outputValue = new SignJWT(inputValue);
                    for(const [key, value] of outputOptions)
                        outputValue = await outputValue[key](...value);
                    break;
                }
            }
            break;
        }
        case 'text': {
            switch(inputType){
                case 'base64': {
                    outputValue = base64.decode(inputValue);
                    break;
                }
            }
            break;
        }
    }
    if(typeof outputValue === 'undefined')
        throw new Error('Not implemented');
    else
        return outputValue;
};

export const types = {
    'base': {
        'base2': {
            'base': {
                'base10': true
            }
        },
        'base10': {
            'base': {
                'base2': {
                    '': true
                }
            }
        }
    },
    'text': {
        '': {
            'base64': {
                '': true
            }
        }
    },
    'date': {
        'milliseconds': {
            'date': {
                'milliseconds': true,
                'seconds': true,
                'text': true
            }
        },
        'text': {
            'date': {
                'milliseconds': true,
                'seconds': true,
                'text': true
            }
        },
        'seconds': {
            'date': {
                'milliseconds': true,
                'seconds': true,
                'text': true
            }
        }
    },
    'jwt': {
        '': {
            'json': {
                '': true
            }
        }
    },
    'json': {
        '': {
            'jwt': {
                '': true
            }
        }
    },
    'base64': {
        '': {
            'text': {
                '': true
            }
        }
    }
};