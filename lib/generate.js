import { nanoid } from 'nanoid';
import {
    v4 as uuid4
} from 'uuid';

export default ({
    outputType,
    outputSubtype,
    outputOptions = []
}) => {
    let outputValue;
    switch(outputType){
        case 'nanoid': {
            outputValue = nanoid(...outputOptions);
            break;
        }
        case 'uuid': {
            switch(outputSubtype){
                case 'v4': {
                    outputValue = uuid4(...outputOptions);
                    break;
                }
            }
            break;
        }
    }
    if(typeof outputValue === 'undefined')
        throw new Error('Not implemented');
    else
        return outputValue;
};

export const types = {
    'nanoid': {
        '': true
    },
    'uuid': {
        'v4': true
    }
};