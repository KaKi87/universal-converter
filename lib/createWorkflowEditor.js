import {
    createStepModel,
    createDefinitionModel,
    createBooleanValueModel
} from 'sequential-workflow-editor-model';

import Prism from 'prismjs';
import jsonata from 'jsonata';

import convert, { types as convertTypes } from './convert.js';
import generate, { types as generateTypes } from './generate.js';
import merge, { types as mergeTypes } from './merge.js';

const
    createFoo = id => ({
        id,
        name: 'test',
        type: 'foo',
        componentType: 'task',
        properties: { isEnabled: true }
    }),
    steps = [
        createStepModel(
            'foo',
            'task',
            step => step
                .property('isEnabled')
                .value(createBooleanValueModel({}))
        )
    ];

export default () => ({
    workflowEditorDefinitionModel: createDefinitionModel(model => {
        model.root(() => {});
        model.steps(steps);
    }),
    workflowEditorToolboxGroups: [
        {
            name: 'Tasks',
            steps: [
                createFoo()
            ]
        }
    ]
});